import mcpi.minecraft as minecraft
import mcpi.block as block
import random, time

mc = minecraft.Minecraft.create()

mc.postToChat("Minecraft Volcano!")

mc.setBlocks(-100, 0, -100, 100, 50, 100, block.AIR)

height = 20
center = 20, 0, 0  # x,y,z

for i in range(height):  # Create the base!
    size = height - i
    mc.setBlocks(center[0] - size, center[1] + i, center[2] - size, center[0] + size, center[1] + i, center[2] + size,
                 block.STONE)

for i in range(height * height):
    randomx = random.randint(center[0] - height, center[0] + height)
    randomz = random.randint(center[2] - height, center[2] + height)
    mc.setBlock(randomx, center[1] + height + 10, randomz, block.GRAVEL)

while True:
    mc.setBlock(center[0], center[1] + height, center[2], block.LAVA_FLOWING)
    time.sleep(1)